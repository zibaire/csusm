﻿using Plugin.Connectivity;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using WebServices.Models;
using Xamarin.Forms;

namespace WebServices
{
	public partial class MainPage : ContentPage
	{

        static HttpClient client = new HttpClient();


        public MainPage()
		{
			InitializeComponent();
            CrossConnectivity.Current.ConnectivityChanged += (sender, e) => {
                if (e.IsConnected)
                {
                    ConnectivityLabel.Text = "Connected to the internet via " + CrossConnectivity.Current.ConnectionTypes.First();
                }
                else
                {
                    ConnectivityLabel.Text = "Not connected to the internet";
                }
            };
        }

        async void Handle_search(object s, EventArgs ev)
        {
            if (CheckInternetConnection() == true)
            {
                var uri = new Uri(
                string.Format(
                    $"https://owlbot.info/api/v2/dictionary/" +
                    $"{((Entry)searchWord).Text.ToLower()}"));

                Debug.WriteLine(uri);

                var request = new HttpRequestMessage();
                request.Method = HttpMethod.Get;
                request.RequestUri = uri;
                request.Headers.Add("Application", "application / json");

                HttpResponseMessage response = await client.SendAsync(request);
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    List<Definitions> definitions = Definitions.FromJson(content);
                    TypeLabel.Text = definitions[0].Type;
                    DefintionLabel.Text = definitions[0].Definition;
                    ExampleLabel.Text = definitions[0].Example;
                    Console.WriteLine(content); 
                }
            }
            }

        public bool CheckInternetConnection()
        {
            if (CrossConnectivity.Current.IsConnected == false)
            {
                DisplayAlert("Alert", "You are not Connected to Internet", "OK");
            }
            return CrossConnectivity.Current.IsConnected;
        }

    }
}