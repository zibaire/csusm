﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace WebServices.Models
{
    public partial class Definitions
    {
        [JsonProperty("type")]
        public string Type { get; set; }
        [JsonProperty("definition")]
        public string Definition { get; set; }
        [JsonProperty("example")]
        public string Example { get; set; }
    }

    public partial class Definitions
    {
        public static List<Definitions> FromJson(string json) => JsonConvert.DeserializeObject<List<Definitions>>(json, WebServices.Models.Converter.Settings);
    }

    public static class Serialize
    {
        public static string ToJson(this List<Definitions> self) => JsonConvert.SerializeObject(self, WebServices.Models.Converter.Settings);
    }

    internal class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters = {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
}
