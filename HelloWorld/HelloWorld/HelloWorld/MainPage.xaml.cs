﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace HelloWorld
{
	public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();
		}


        private void Linkedin_clicked(object sender, EventArgs e)
        {
            Debug.WriteLine("Linkedin");
            Device.OpenUri(new Uri("http://www.linkedin.com/in/antoine-maciazek-239b06b5"));
        }
    }
}
